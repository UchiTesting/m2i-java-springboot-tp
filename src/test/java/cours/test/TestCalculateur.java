package cours.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tp.appliSpring.core.service.ServiceCompteTest;

public class TestCalculateur {

	private Calculateur calculateur; // Objet à tester
	private static Logger logger = LoggerFactory.getLogger(ServiceCompteTest.class);

	public TestCalculateur() {
		logger.debug("ctor test appelé. " + this);
	}

	@BeforeEach
	public void init() {
		calculateur = new Calculateur();
		logger.debug("BeforeEach appelé. " + this);
	}

	@Test
	public void testSomme() {
		logger.debug("testSomme appelé. " + this);
		calculateur.addVal(8);
		calculateur.addVal(2);
		double s = calculateur.getSomme();

		Assertions.assertEquals(10.0, s, 0.000001);
	}

	@Test
	public void testN() {
		logger.debug("testN appelé. " + this);
		calculateur.addVal(7);
		calculateur.addVal(5);
		double n = calculateur.getN();

		Assertions.assertTrue(n == 2);
	}

	@Test
	public void testMoyenne() {
		logger.debug("testMoyenne appelé. " + this);
		calculateur.addVal(1);
		calculateur.addVal(10);
		calculateur.addVal(100);
		double result = calculateur.moyenne();

		Assertions.assertTrue(result == 37);
	}
}
