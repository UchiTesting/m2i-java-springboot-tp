package cours.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tp.appliSpring.core.service.ServiceCompteTest;

// Tester des fonctions "à état" dans un jeu de test `static` risque de 
// causer des problèmes car les données sont préservées entre tests
public class TestCalculateurStatic {

	private static Calculateur calculateur; // Objet à tester
	private static Logger logger = LoggerFactory.getLogger(ServiceCompteTest.class);

	public TestCalculateurStatic() {
		logger.debug("ctor test appelé. " + this);
	}

	@BeforeAll
	// Parce que les fonctions à tester ne dépendent pas de l'état, on peut utiliser
	// l'approche avec static
	public static void init() {
		calculateur = new Calculateur();
		logger.debug("BeforeAll appelé. ");
	}

	@Test
	public void testCarre() {
		logger.debug("testCarre appelé. " + this);
		Assertions.assertEquals(16, calculateur.carre(4), 0.000001);
	}

	@Test
	public void testCarre2() {
		logger.debug("testCarre2 appelé. " + this);
		Assertions.assertEquals(9, calculateur.carre(3), 0.000001);
	}

	@Test
	public void testRacineCarre() {
		logger.debug("testRacineCarre appelé. " + this);
		Assertions.assertEquals(5, calculateur.racineCarre(25));
	}

	@Test
	public void testRacineCarre2() {
		logger.debug("testRacineCarre appelé. " + this);
		Assertions.assertEquals(11, calculateur.racineCarre(121));
	}
}
