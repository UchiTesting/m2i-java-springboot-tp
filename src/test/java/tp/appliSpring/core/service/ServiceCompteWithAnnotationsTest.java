package tp.appliSpring.core.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import tp.appliSpring.AppliSpringBootApplication;
import tp.appliSpring.core.entity.Compte;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes= {AppliSpringBootApplication.class})
@ActiveProfiles({"dev"}) // Va tenir compte du fichier application-dev.properties
public class ServiceCompteWithAnnotationsTest {

	@Autowired
	private IServiceCompte serviceCompte;

	@Test
	public void testRechercherCompte() {
		Compte compte1 = new Compte(null, "Compte 1", 200.0);

		serviceCompte.sauvegarderCompte(compte1);
		Long nouvelId = compte1.getNumero(); // Numero auto-incrémenté
		System.out.println("Nouvel ID => " + nouvelId);

		Compte compte1InDb = serviceCompte.rechercherCompteParNumero(nouvelId);
		System.out.println("Compte Test => " + compte1InDb);

		Assertions.assertTrue(compte1InDb.getNumero() == nouvelId);

	}
}
