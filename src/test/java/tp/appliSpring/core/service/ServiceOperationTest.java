package tp.appliSpring.core.service;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import tp.appliSpring.AppliSpringBootApplication;
import tp.appliSpring.core.dao.IDaoOperation;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.entity.Operation;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = { AppliSpringBootApplication.class })
@ActiveProfiles({ "dev" }) // Va tenir compte du fichier application-dev.properties
public class ServiceOperationTest {

	@Autowired
	private IDaoOperation daoOperation;
	@Autowired
	private IServiceCompte serviceCompte;

	AnnotationConfigApplicationContext springContext;

	private static Logger logger = LoggerFactory.getLogger(ServiceOperationTest.class);

	@Test
	public void testRecuperationOperationParNumeroCompte() {
		Compte compte1 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 1", 100.0));
		Compte compte2 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 2", 100.0));

		daoOperation.save(new Operation(null, "Operation 1", 0.0, new Date(), compte1));
		daoOperation.save(new Operation(null, "Operation 2", 0.0, new Date(), compte2));
		daoOperation.save(new Operation(null, "Operation 3", 0.0, new Date(), compte1));

		int result = daoOperation.findOperationsByCompteNumero(compte1.getNumero()).size();
		logger.debug("Operations for account "+compte1.getNumero()+" => "+result);

		Assertions.assertEquals(2, result);
	}
}
