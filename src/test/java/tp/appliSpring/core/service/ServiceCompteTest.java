package tp.appliSpring.core.service;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import tp.appliSpring.core.config.ServiceAndDaoConfig;
import tp.appliSpring.core.dao.IDaoOperation;
import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.entity.Operation;
import tp.appliSpring.core.exception.NotFoundException;
import tp.appliSpring.core.exception.SoldeInsuffisantException;

@ExtendWith(SpringExtension.class)
//@SpringBootTest(classes= {AppliSpringBootApplication.class})
@SpringBootTest(classes = { ServiceAndDaoConfig.class })
@ActiveProfiles({ "dev" }) // Va tenir compte du fichier application-dev.properties
public class ServiceCompteTest {

	@Autowired
	private IServiceCompte serviceCompte;
	@Autowired
	private IServiceClient serviceClient;
	@Autowired
	private IDaoOperation daoOperation;

	AnnotationConfigApplicationContext springContext;

	private static Logger logger = LoggerFactory.getLogger(ServiceCompteTest.class);

	@Test
	public void testRechercherCompte() {
		Compte compte1 = new Compte(null, "Compte 1", 200.0);

		serviceCompte.sauvegarderCompte(compte1);
		Long nouvelId = compte1.getNumero(); // Numero auto-incrémenté
		System.out.println("Nouvel ID => " + nouvelId);

		Compte compte1InDb = serviceCompte.rechercherCompteParNumero(nouvelId);
		System.out.println("Compte Test => " + compte1InDb);

		Assertions.assertTrue(compte1InDb.getNumero() == nouvelId);

	}

	@Test
	public void testRechercherComptesClient() {

		Compte compte1 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 1", 200.0));
		Compte compte2 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 2", 150.0));
		Client client1 = serviceClient
				.sauvegarderClient(new Client(null, "nom", "prenom", "adresse", "client@domain.com"));

		client1.getComptes().add(compte1);
		client1.getComptes().add(compte2);
		serviceClient.sauvegarderClient(client1);

		List<Compte> comptes = serviceCompte.rechercherComptesDuClient(client1.getNumero());
		logger.debug("Comptes du client => " + comptes);

		Compte compte3 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 3", 10.0));
		Compte compte4 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 4", 150.0));
		Client client2 = serviceClient
				.sauvegarderClient(new Client(null, "nom2", "prenom2", "adresse2", "client2@domain.com"));

		client2.getComptes().add(compte3);
		client2.getComptes().add(compte4);
		serviceClient.sauvegarderClient(client2);

		comptes = serviceCompte.rechercherComptesDuClient(client2.getNumero());
		logger.debug("Comptes du client => " + comptes);
	}

	@Test
	public void testGetAllComptes() {
		Compte compte1 = new Compte(null, "Compte 1", 200.0);
		Compte compte2 = new Compte(null, "Compte 2", 150.5);

		serviceCompte.sauvegarderCompte(compte1);
		serviceCompte.sauvegarderCompte(compte2);
		List<Compte> comptesInDb = serviceCompte.rechercherTousComptes();
		logger.debug("Comptes en DB => " + comptesInDb.size());
		logger.debug("Comptes => " + comptesInDb);

		Assertions.assertTrue(comptesInDb.size() > 0);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testAddOperations() {
		Compte compte = serviceCompte.sauvegarderCompte(new Compte(null, "Compte Ops", 200.0));

		Operation operation1 = new Operation(null, "Operation 1", 100.2, new Date(22, 11, 07, 12, 00, 00), compte);
		Operation operation2 = new Operation(null, "Operation 2", -21.4, new Date(22, 11, 07, 12, 01, 00), compte);
		Operation operation3 = new Operation(null, "Operation 3", 58.7, new Date(22, 11, 07, 12, 02, 00), compte);

		operation1.setCompte(compte);
		operation2.setCompte(compte);
		operation3.setCompte(compte);

		daoOperation.save(operation1);
		daoOperation.save(operation2);
		daoOperation.save(operation3);

		List<Operation> operationsFromDb = daoOperation.findOperationsByCompteNumero(compte.getNumero());
		logger.debug("truc " + operationsFromDb);
	}

	@Test
	public void testBonVirement() {
		Compte compteASauvegarde = this.serviceCompte.sauvegarderCompte(new Compte(null, "compteA", 300.0));
		Compte compteBSauvegarde = this.serviceCompte.sauvegarderCompte(new Compte(null, "compteB", 100.0));

		long numCptA = compteASauvegarde.getNumero();
		long numCptB = compteBSauvegarde.getNumero();

		// remonter en memoire les anciens soldes des compte A et B avant virement
		// (+affichage console ou logger) :
		double soldeA_avant = compteASauvegarde.getSolde();
		double soldeB_avant = compteBSauvegarde.getSolde();
		logger.debug("avant bon virement, soldeA_avant=" + soldeA_avant + " et soldeB_avant=" + soldeB_avant);

		// effectuer un virement de 50 euros d'un compte A vers vers compte B
		this.serviceCompte.transferer(50.0, numCptA, numCptB);

		// remonter en memoire les nouveaux soldes des compte A et B apres virement
		// (+affichage console ou logger)
		Compte compteAReluApresVirement = this.serviceCompte.rechercherCompteParNumero(numCptA);
		Compte compteBReluApresVirement = this.serviceCompte.rechercherCompteParNumero(numCptB);

		double soldeA_apres = compteAReluApresVirement.getSolde();
		double soldeB_apres = compteBReluApresVirement.getSolde();

		logger.debug("apres bon virement, soldeA_apres=" + soldeA_apres + " et soldeB_apres=" + soldeB_apres);

		// verifier -50 et +50 sur les différences de soldes sur A et B :
		Assertions.assertEquals(soldeA_avant - 50, soldeA_apres, 0.000001);
		Assertions.assertEquals(soldeB_avant + 50, soldeB_apres, 0.000001);
	}

	@Test
	public void testMauvaisVirement() {
		Compte compteASauvegarde = this.serviceCompte.sauvegarderCompte(new Compte(null, "compteA", 300.0));
		Compte compteBSauvegarde = this.serviceCompte.sauvegarderCompte(new Compte(null, "compteB", 100.0));

		long numCptA = compteASauvegarde.getNumero();
		long numCptB = compteBSauvegarde.getNumero();

		// remonter en memoire les anciens soldes des compte A et B avant virement
		// (+affichage console ou logger) :
		double soldeA_avant = compteASauvegarde.getSolde();
		double soldeB_avant = compteBSauvegarde.getSolde();
		logger.debug("avant mauvais virement, soldeA_avant=" + soldeA_avant + " et soldeB_avant=" + soldeB_avant);

		// effectuer un virement de 50 euros d'un compte A vers vers compte B
		try {
			this.serviceCompte.transferer(50.0, numCptA, -numCptB);
		} catch (Exception e) {
			logger.error("Echec prévu par le test => OK. " + e.getMessage());
		}

		// remonter en memoire les nouveaux soldes des compte A et B apres virement
		// (+affichage console ou logger)
		Compte compteAReluApresVirement = this.serviceCompte.rechercherCompteParNumero(numCptA);
		Compte compteBReluApresVirement = this.serviceCompte.rechercherCompteParNumero(numCptB);

		double soldeA_apres = compteAReluApresVirement.getSolde();
		double soldeB_apres = compteBReluApresVirement.getSolde();

		logger.debug("apres mauvais virement, soldeA_apres=" + soldeA_apres + " et soldeB_apres=" + soldeB_apres);

		// verifier -50 et +50 sur les différences de soldes sur A et B :
		Assertions.assertEquals(soldeA_avant, soldeA_apres, 0.000001);
		Assertions.assertEquals(soldeB_avant, soldeB_apres, 0.000001);
	}

	@Test
	public void testDecouvertVirement() {
		Assertions.assertThrows(SoldeInsuffisantException.class, () -> {

			Compte compteASauvegarde = this.serviceCompte.sauvegarderCompte(new Compte(null, "compteA", 300.0));
			Compte compteBSauvegarde = this.serviceCompte.sauvegarderCompte(new Compte(null, "compteB", 100.0));

			long numCptA = compteASauvegarde.getNumero();
			long numCptB = compteBSauvegarde.getNumero();

			// remonter en memoire les anciens soldes des compte A et B avant virement
			// (+affichage console ou logger) :
			double soldeA_avant = compteASauvegarde.getSolde();
			double soldeB_avant = compteBSauvegarde.getSolde();
			logger.debug("avant bon virement, soldeA_avant=" + soldeA_avant + " et soldeB_avant=" + soldeB_avant);

			// effectuer un virement de 50 euros d'un compte A vers vers compte B
			this.serviceCompte.transferer(500.0, numCptA, numCptB);

			// remonter en memoire les nouveaux soldes des compte A et B apres virement
			// (+affichage console ou logger)
			Compte compteAReluApresVirement = this.serviceCompte.rechercherCompteParNumero(numCptA);
			Compte compteBReluApresVirement = this.serviceCompte.rechercherCompteParNumero(numCptB);

			double soldeA_apres = compteAReluApresVirement.getSolde();
			double soldeB_apres = compteBReluApresVirement.getSolde();

			logger.debug("apres bon virement, soldeA_apres=" + soldeA_apres + " et soldeB_apres=" + soldeB_apres);

			// verifier -50 et +50 sur les différences de soldes sur A et B :
			Assertions.assertEquals(soldeA_avant, soldeA_apres, 0.000001);
			Assertions.assertEquals(soldeB_avant, soldeB_apres, 0.000001);
		});

	}

	// @Test
	// To be fixed
	public void testComptesPositif() {
		int nbComptes = serviceCompte.rechercherTousComptes().size();
		logger.debug("NB Comptes => " + nbComptes);
		Assertions.assertEquals(5, serviceCompte.rechercherTousComptes().size());
		Assertions.assertEquals(5, serviceCompte.rechercherComptesSoldeMin(-1).size());
		Assertions.assertEquals(5, serviceCompte.rechercherComptesSoldeMin(0).size());
		Assertions.assertEquals(4, serviceCompte.rechercherComptesSoldeMin(100).size());
		Assertions.assertEquals(3, serviceCompte.rechercherComptesSoldeMin(200).size());
		Assertions.assertEquals(2, serviceCompte.rechercherComptesSoldeMin(300).size());
		Assertions.assertEquals(1, serviceCompte.rechercherComptesSoldeMin(400).size());
	}

	// @Test
	// To be fixed
	public void testCompteAvecOperations() {
		Compte compte = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 1", 0.0));

		Operation operation1 = new Operation(null, "Operation 1", 100.2, new Date(22, 11, 07, 12, 00, 00), compte);
		Operation operation2 = new Operation(null, "Operation 2", -21.4, new Date(22, 11, 07, 12, 01, 00), compte);
		Operation operation3 = new Operation(null, "Operation 3", 58.7, new Date(22, 11, 07, 12, 02, 00), compte);

		logger.debug("Compte =>" + compte);
		logger.debug("COMPTE OPERATIONS => " + compte.getOperations());

		daoOperation.save(operation1);
		daoOperation.save(operation2);
		daoOperation.save(operation3);

		serviceCompte.rechercherCompteAvecOperationsParNumero(1L);

		logger.debug("COMPTE OPERATIONS => " + compte);
		logger.debug("COMPTE OPERATIONS => " + compte.getOperations());

		Assertions.assertEquals(3, compte.getOperations().size());
	}

	@Test
	public void testVerifDecouvert() {
		Compte compteSauvegarde = serviceCompte.sauvegarderCompte(new Compte(null, "ComptePositif", 256.0));
		boolean bPasDecouvert = serviceCompte.verifDecouvert(compteSauvegarde.getNumero());
		logger.debug("bPasDecouvert=" + bPasDecouvert);
		Assertions.assertTrue(bPasDecouvert);

		Compte compteSauvegarde2 = serviceCompte.sauvegarderCompte(new Compte(null, "CompteNegatif", -256.0));
		boolean bPasDecouvert2 = serviceCompte.verifDecouvert(compteSauvegarde2.getNumero());
		logger.debug("bPasDecouvert2=" + bPasDecouvert2);
		Assertions.assertFalse(bPasDecouvert2);
	}

	@Test
	public void testRechercherCompteParNumero() {
		Compte compteInDb = serviceCompte.rechercherCompteParNumero(1);

		logger.debug("Compte par numero " + compteInDb);

	}

	@Test
	public void testDelete() {
		Compte newCompteToBeDeleted = serviceCompte.sauvegarderCompte(new Compte(null, "To be deleted", 0.0));

		if (newCompteToBeDeleted == null)
			Assertions.fail("Compte inexistant");

		logger.debug("About to DELETE...");

		serviceCompte.supprimerCompte(newCompteToBeDeleted.getNumero());
		Compte newCompteInDb = null;
		
		try {
			newCompteInDb = serviceCompte.rechercherCompteParNumero(newCompteToBeDeleted.getNumero());
		} catch (NotFoundException e) {
			Assertions.assertTrue(newCompteInDb == null, "Account successfully deleted");
		}
		
	}
}
