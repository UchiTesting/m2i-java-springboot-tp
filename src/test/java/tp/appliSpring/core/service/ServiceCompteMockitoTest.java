package tp.appliSpring.core.service;

import static org.mockito.ArgumentMatchers.anyLong;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tp.appliSpring.core.dao.IDaoCompte;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.exception.NotFoundException;

@ExtendWith(MockitoExtension.class)
public class ServiceCompteMockitoTest {

	@InjectMocks // Objet réel
	// Mettre un type d'implémentation.
	private ServiceCompteImpl serviceCompte;

	@Mock // Mock
	private IDaoCompte daoCompteMock;

	private static Logger logger = LoggerFactory.getLogger(ServiceCompteMockitoTest.class);

	@Test
	public void testVerifDecouvert() {
		Long numCompte = 501L;
		Mockito.when(daoCompteMock.findById(numCompte))
				.thenReturn(Optional.of(new Compte(null, "ComptePositif", 256.0)));

		boolean bPasDecouvert = serviceCompte.verifDecouvert(numCompte);
		logger.debug("bPasDecouvert=" + bPasDecouvert);
		Assertions.assertTrue(bPasDecouvert);

	}

	@Test
	public void testVerifPasDecouvert() {
		Long numCompte = 502L;
		Mockito.when(daoCompteMock.findById(numCompte))
				.thenReturn(Optional.of(new Compte(null, "CompteNegatif", -256.0)));
		boolean bPasDecouvert2 = serviceCompte.verifDecouvert(numCompte);
		logger.debug("bPasDecouvert2=" + bPasDecouvert2);
		Assertions.assertFalse(bPasDecouvert2);
	}

	@Test
	public void testVerifPasDecouvertCompteInconnu() {
		Long numCompteInconnu = 999L;
		Mockito.when(daoCompteMock.findById(numCompteInconnu)).thenReturn(Optional.empty());

		boolean bPasDecouvert2;

		try {
			bPasDecouvert2 = serviceCompte.verifDecouvert(numCompteInconnu);
			Assertions.fail("Exception non remontée");
		} catch (NotFoundException e) {
			logger.debug("Exception remontée " + e.getMessage());
			Assertions.assertTrue(e.getClass().getSimpleName().equals(NotFoundException.class.getSimpleName()));
		}
	}

	@Test
	public void testVerifPasDecouvertVerifAppelDaoFindById() {
		Long numCompte = 502L;
		Mockito.when(daoCompteMock.findById(numCompte))
				.thenReturn(Optional.of(new Compte(null, "Compte Negatif", -256.0)));
		boolean bPasDecouvert2 = serviceCompte.verifDecouvert(numCompte);
		logger.debug("bPasDecouvert2=" + bPasDecouvert2);
		Assertions.assertFalse(bPasDecouvert2);
		Mockito.verify(daoCompteMock,Mockito.times(1)).findById(anyLong());
	}
}
