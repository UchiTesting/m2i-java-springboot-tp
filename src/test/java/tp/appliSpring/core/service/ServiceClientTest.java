package tp.appliSpring.core.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import tp.appliSpring.AppliSpringBootApplication;
import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.entity.Compte;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes= {AppliSpringBootApplication.class})
@ActiveProfiles({"dev"}) // Va tenir compte du fichier application-dev.properties
public class ServiceClientTest {

	@Autowired
	private IServiceClient serviceClient;
	@Autowired
	private IServiceCompte serviceCompte;
	AnnotationConfigApplicationContext springContext;

	private static Logger logger = LoggerFactory.getLogger(ServiceClientTest.class);

	@Test
	public void testRechercherClient() {
		Client Client1 = new Client(null, "Dupont", "Jean", "1 rue des client1", "j.dupont@domain.com");

		serviceClient.sauvegarderClient(Client1);
		Long nouvelId = Client1.getNumero(); // Numero auto-incrémenté
		System.out.println("Nouvel ID => " + nouvelId);

		Client Client1InDb = serviceClient.rechercherClientParNumero(nouvelId);
		System.out.println("Client Test => " + Client1InDb);

		Assertions.assertTrue(Client1InDb.getNumero() == nouvelId);

	}

	@Test
	public void testModifierClient() {
		Client Client1 = new Client(null, "Dupont", "Jean", "1 rue des client1", "j.dupont@domain.com");

		serviceClient.sauvegarderClient(Client1);
		Long nouvelId = Client1.getNumero(); // Numero auto-incrémenté
		System.out.println("Nouvel ID => " + nouvelId);

		Client clientFromDb = serviceClient.rechercherClientParNumero(nouvelId);

		String nouvelleAdresse = "10 rue des modifies";
		clientFromDb.setAdresse(nouvelleAdresse);
		serviceClient.sauvegarderClient(clientFromDb);

		clientFromDb = null;

		clientFromDb = serviceClient.rechercherClientParNumero(nouvelId);

		Assertions.assertEquals(nouvelId, clientFromDb.getNumero());
		Assertions.assertEquals(nouvelleAdresse, clientFromDb.getAdresse());
		logger.debug("Client from DB => " + clientFromDb);
	}

	@Test
	public void testSupprimerClient() {
		Client Client1 = new Client(null, "Dupont", "Jean", "1 rue des client1", "j.dupont@domain.com");

		serviceClient.sauvegarderClient(Client1);
		Long nouvelId = Client1.getNumero(); // Numero auto-incrémenté
		System.out.println("Nouvel ID => " + nouvelId);

		Client clientFromDb = serviceClient.rechercherClientParNumero(nouvelId);
		logger.debug("Client from DB => ", clientFromDb);

		Integer nbOfClients = serviceClient.rechercherTousClients().size();

		logger.debug("Clients before deletion =>" + nbOfClients);

		serviceClient.supprimerClient(nouvelId);
//		serviceClient.sauvegarderClient(clientFromDb);

		nbOfClients = serviceClient.rechercherTousClients().size();
		logger.debug("Clients after deletion =>" + nbOfClients);

		Assertions.assertEquals(null, serviceClient.rechercherClientParNumero(nouvelId));
	}

	@Test
	public void testGetAllClients() {
		Client client1 = serviceClient.sauvegarderClient(new Client(null, "Dupont", "Jean", "1 rue des client1", "j.dupont@domain.com"));
		Client client2 = serviceClient.sauvegarderClient(new Client(null, "Leblanc", "Pascal", "1 rue des client1", "p.leblanc@domain.com"));
		Client client3 = serviceClient.sauvegarderClient(new Client(null, "Martin", "Pierre", "1 rue des client1", "p.martin@domain.com"));

		// TODO
//		int client1Id = (int)client1.getNumero();
//
//
//		List<Client> clientsInDb = serviceClient.rechercherTousClients();
//		logger.debug("Clients en DB => " + clientsInDb.size());
//		logger.debug("Clients => " + clientsInDb);
//
//		Assertions.assertEquals(client1.getNom(), clientsInDb.get().getNom());
//		Assertions.assertEquals(client2.getNom(), clientsInDb.get(client2.getNumero()).getNom());
//		Assertions.assertEquals(client3.getNom(), clientsInDb.get(client3.getNumero()).getNom());
	}

	@Test
	public void testRechercherClientAvecComptes() {
		Compte compte1 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 1", 200.0));
		Compte compte2 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 2", 150.0));

		Client client1 = new Client(null, "nom", "prenom", "adresse", "client@domain.com");

		client1.getComptes().add(compte1);
		client1.getComptes().add(compte2);

		serviceClient.sauvegarderClient(client1);

		Client clientRelu = serviceClient.rechercherClientParNumeroInclusComptes(client1.getNumero());

		logger.debug("Client relu => " + clientRelu);

		for (Compte cpt : clientRelu.getComptes()) {
			logger.debug("\t" + cpt);
		}
	}

}
