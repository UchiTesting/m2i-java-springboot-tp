package tp.appliSpring;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles({"dev"}) // Va tenir compte du fichier application-dev.properties
class AppliSpringBootApplicationTests {

	@Test
	void contextLoads() {
	}

}
