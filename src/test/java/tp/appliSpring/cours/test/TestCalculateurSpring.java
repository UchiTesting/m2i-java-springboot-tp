package tp.appliSpring.cours.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import tp.appliSpring.core.service.ServiceCompteTest;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes= {ConfigLegereTest.class})
@ActiveProfiles({"dev"})
@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class TestCalculateurSpring {
	@Autowired
	private CalculateurSpring calculateur;
	private static Logger logger = LoggerFactory.getLogger(ServiceCompteTest.class);
	
	@Test
	public void testSomme() {
		logger.debug("testSomme appelé. " + this);
		calculateur.addVal(8);
		calculateur.addVal(2);
		double s = calculateur.getSomme();

		Assertions.assertEquals(10.0, s, 0.000001);
	}
	
	@Test
	public void testMoyenne() {
		logger.debug("testMoyenne appelé. " + this);
		calculateur.addVal(1);
		calculateur.addVal(10);
		calculateur.addVal(100);
		double result = calculateur.moyenne();

		Assertions.assertTrue(result == 37);
	}
}
