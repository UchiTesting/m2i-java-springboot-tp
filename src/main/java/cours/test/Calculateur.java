package cours.test;

public class Calculateur {
	private double somme;
	private int n;

	public Calculateur() {
		this.somme = 0;
		this.n = 0;
	}

	public double getSomme() {
		return somme;
	}

	public void setSomme(double somme) {
		this.somme = somme;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public double carre(double x) {
		return x * x;
	}

	public void addVal(double x) {
		somme += x;
		n++;
	}

	public double racineCarre(double x) {
		return Math.sqrt(x);
	}

	public double moyenne() {
		return somme / n;
	}
}
