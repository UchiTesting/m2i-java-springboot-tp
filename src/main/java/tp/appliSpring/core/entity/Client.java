package tp.appliSpring.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tp.appliSpring.core.config.JpaConstants;

@Entity
@Table(name = "clients")
@NamedQuery(name = JpaConstants.clientFindAll, query = JpaConstants.clientFindAllQuery)
@NamedQuery(name = JpaConstants.clientFindClientByIdIncludeAccounts, query = JpaConstants.clientFindClientByIdIncludeAccountsQuery)
@Getter
@Setter
@NoArgsConstructor
public class Client {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long numero;
	private String nom;
	private String prenom;
	private String adresse;
	private String email;

	// Client est le côté principal
	@ManyToMany
	@JoinTable(name = "Clients_Comptes", joinColumns = { @JoinColumn(name = "numClient") }, inverseJoinColumns = {
			@JoinColumn(name = "numCompte") })
	// Pour ignorer .comptes lorsque le client java sera transformé en client json
	// MAIS c'est beaucoup moins bien que les DTO
	@JsonIgnore
	private List<Compte> comptes = new ArrayList<>();

	public Client(Long numero, String nom, String prenom, String adresse, String email) {
		super();
		this.numero = numero;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.email = email;
	}

	@Override
	public String toString() {
		return "Client [numero=" + numero + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse + ", email="
				+ email + "]";
	}
}
