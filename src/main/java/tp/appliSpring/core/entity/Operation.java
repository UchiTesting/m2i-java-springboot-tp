package tp.appliSpring.core.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tp.appliSpring.core.config.JpaConstants;

@Entity
@Table(name = "operations")
@NamedQuery(name = JpaConstants.operationFindAll, query = JpaConstants.operationFindAllQuery)
@NamedQuery(name = JpaConstants.operationFindOperationsByAccountId, query = JpaConstants.operationFindOperationsByAccountIdQuery)
public class Operation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long numOp;
	String label;
	Double montant;
	@Temporal(TemporalType.DATE)
	Date dateOp;
	
	@ManyToOne
	@JoinColumn(name="numCompte")
	Compte compte;
	
	public Operation() {
		super();
	}
	
	public Operation(Long numOp, String label, Double montant, Date dateOp, Compte compte) {
		super();
		this.numOp = numOp;
		this.label = label;
		this.montant = montant;
		this.dateOp = dateOp;
		this.compte = compte;
	}

	public Long getNumOp() {
		return numOp;
	}

	public void setNumOp(Long numOp) {
		this.numOp = numOp;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

	public Date getDateOp() {
		return dateOp;
	}

	public void setDateOp(Date dateOp) {
		this.dateOp = dateOp;
	}

	public Compte getCompte() {
		return compte;
	}

	public void setCompte(Compte compte) {
		this.compte = compte;
	}

	@Override
	public String toString() {
		return "Operation [numOp=" + numOp + ", label=" + label + ", montant=" + montant + ", dateOp=" + dateOp + "]";
	}
}
