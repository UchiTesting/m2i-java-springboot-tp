package tp.appliSpring.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import tp.appliSpring.core.config.JpaConstants;

@Entity
@Table(name = "comptes")
@NamedQuery(name = JpaConstants.compteFindAll, query = JpaConstants.compteFindAllQuery)
@NamedQuery(name = JpaConstants.compteFindByClientId, query = JpaConstants.compteFindByClientIdQuery)
@NamedQuery(name= JpaConstants.compteFindWithOperationsById,query=JpaConstants.compteFindWithOperationsByIdQuery)
public class Compte {
	@Id
	// Pour que l'id auto incrémenté par la base remonte en mémoire dans la classe
	// Java
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long numero;

	@Column(name = "label", length = 50)
	private String label;
	private Double solde;

	@ManyToMany(mappedBy = "comptes")
	private List<Client> clients = new ArrayList<>();

	@OneToMany(mappedBy = "compte")
	private List<Operation> operations = new ArrayList<>();

	public Compte() {
	}

	public Compte(Long numero, String label, Double solde) {
		super();
		this.numero = numero;
		this.label = label;
		this.solde = solde;
	}

	public List<Client> getClients() {
		return clients;
	}

	public String getLabel() {
		return label;
	}

	public Long getNumero() {
		return numero;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public Double getSolde() {
		return solde;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public void setSolde(Double solde) {
		this.solde = solde;
	}

	@Override
	public String toString() {
		return "Compte [numero=" + numero + ", label=" + label + ", solde=" + solde + "]";
	}

}
