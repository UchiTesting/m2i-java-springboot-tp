package tp.appliSpring.core.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tp.appliSpring.core.dao.IDaoCompte;
import tp.appliSpring.core.dao.IDaoOperation;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.entity.Operation;
import tp.appliSpring.core.exception.NotFoundException;
import tp.appliSpring.core.exception.SoldeInsuffisantException;

@Service
@Transactional
public class ServiceCompteImpl implements IServiceCompte {

	private IDaoCompte daoCompte;
	private IDaoOperation daoOperation;

	public ServiceCompteImpl() {
	}

	@Autowired
	public ServiceCompteImpl(IDaoCompte daoCompte, IDaoOperation daoOperation) {
		this.daoCompte = daoCompte;
		this.daoOperation = daoOperation;
	}

	@Override
	public Compte rechercherCompteParNumero(long numero) throws NotFoundException {
		Optional<Compte> optCompte = daoCompte.findById(numero);
		if (optCompte.isEmpty())
			throw new NotFoundException("compte not found avec numero=" + numero);
		/* else */
		return optCompte.get();
	}

	@Override
	public List<Compte> rechercherTousComptes() {
		return daoCompte.findAll();
	}

	@Override
	public List<Compte> rechercherComptesDuClient(long numClient) {
		return daoCompte.findByClientId(numClient);
	}

	@Override
	public Compte sauvegarderCompte(Compte compte) {
		return daoCompte.save(compte);
	}

	@Override
	public void supprimerCompte(long numCpt) {
		daoCompte.deleteById(numCpt);
	}

	@Override
	public void transferer(double montant, long numCptDeb, long numCptCred) {
		try {
			Compte cptDebt = daoCompte.findById(numCptDeb).orElse(null);

			if (cptDebt.getSolde() < montant)
				throw new SoldeInsuffisantException("Solde insuffisant pour le compte " + numCptDeb);

			cptDebt.setSolde(cptDebt.getSolde() - montant);
			daoOperation.save(
					new Operation(null, "Virement débiteur vers compte " + numCptCred, montant, new Date(), cptDebt));

			Compte cptCred = daoCompte.findById(numCptCred).orElse(null);
			cptCred.setSolde(cptCred.getSolde() + montant);
			daoOperation.save(
					new Operation(null, "Virement créditeur depuis compte " + numCptDeb, montant, new Date(), cptCred));
		} catch (SoldeInsuffisantException e) {
			System.err.println(e.getMessage());
			throw e;
		} catch (Exception e) {
			// Provoque le rollback
			throw new RuntimeException("Echec du virement", e);
		}
	}

	@Override
	public List<Compte> rechercherComptesSoldeMin(double soldeMin) {
		return daoCompte.findBySoldeGreaterThanEqual(soldeMin);
	}

	public Compte rechercherCompteAvecOperationsParNumero(Long numCompte) {
		return daoCompte.findWithOperationsById(numCompte);
	}

	@Override
	public boolean verifDecouvert(long numero) throws NotFoundException {
		Compte compte = daoCompte.findById(numero).orElse(null);

		if (compte == null)
			throw new NotFoundException("Compte " + numero + " inexistant.");

		return compte.getSolde() >= 0.0;
	}

}