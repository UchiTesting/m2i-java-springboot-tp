package tp.appliSpring.core.service;

import java.util.List;

import tp.appliSpring.core.entity.Operation;

public interface IServiceOperation {
	Operation rechercherOperationParNumero(long numOpt);

	List<Operation> rechercherTousOperations();

	List<Operation> rechercherOperationsDuCompte(long numCompte);

	Operation sauvegarderOperation(Operation Operation);

	void supprimerOperation(long numOpt);
}
