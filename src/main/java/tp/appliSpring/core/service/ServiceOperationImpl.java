package tp.appliSpring.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp.appliSpring.core.dao.IDaoOperation;
import tp.appliSpring.core.entity.Operation;

@Service
public class ServiceOperationImpl implements IServiceOperation {

	IDaoOperation daoOperation;

	public ServiceOperationImpl() {
		super();
	}

	@Autowired
	public ServiceOperationImpl(IDaoOperation daoOperation) {
		this.daoOperation = daoOperation;
	}

	@Override
	public Operation rechercherOperationParNumero(long numOpt) {
		return daoOperation.findById(numOpt).orElse(null);
	}

	@Override
	public List<Operation> rechercherTousOperations() {
		return daoOperation.findAll();
	}

	@Override
	public List<Operation> rechercherOperationsDuCompte(long numCompte) {
		return daoOperation.findOperationsByCompteNumero(numCompte);
	}

	@Override
	public Operation sauvegarderOperation(Operation Operation) {
		daoOperation.save(Operation);
		return null;
	}

	@Override
	public void supprimerOperation(long numOpt) {
		daoOperation.deleteById(numOpt);

	}

}
