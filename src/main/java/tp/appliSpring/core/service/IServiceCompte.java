package tp.appliSpring.core.service;

import java.util.List;

import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.exception.NotFoundException;

public interface IServiceCompte {
	Compte rechercherCompteParNumero(long numero) throws NotFoundException;

	List<Compte> rechercherTousComptes();

	List<Compte> rechercherComptesDuClient(long numClient);

	Compte sauvegarderCompte(Compte compte);

	void supprimerCompte(long numCpt);

	void transferer(double montant, long numCptDeb, long numCptCred);

	List<Compte> rechercherComptesSoldeMin(double soldeMin);

	Compte rechercherCompteAvecOperationsParNumero(Long numCompte);

	boolean verifDecouvert(long numero) throws NotFoundException;
}
