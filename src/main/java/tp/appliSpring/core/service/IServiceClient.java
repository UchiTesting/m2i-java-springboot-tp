package tp.appliSpring.core.service;

import java.util.List;

import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.entity.Compte;

public interface IServiceClient {
	Client rechercherClientParNumero(long numero);

	Client rechercherClientParNumeroInclusComptes(long numero);

	List<Client> rechercherTousClients();

	List<Compte> rechercherComptesDuClient(long numClient);

	Client sauvegarderClient(Client Client);

	void supprimerClient(long numClt);
}
