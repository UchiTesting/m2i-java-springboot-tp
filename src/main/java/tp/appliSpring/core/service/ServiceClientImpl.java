package tp.appliSpring.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tp.appliSpring.core.dao.IDaoClient;
import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.entity.Compte;

@Service
public class ServiceClientImpl implements IServiceClient {

	private IDaoClient daoClient;

	public ServiceClientImpl() {
	}

	@Autowired
	public ServiceClientImpl(IDaoClient daoClient) {
		this.daoClient = daoClient;
	}

	@Override
	public Client rechercherClientParNumero(long numero) {
		return daoClient.findById(numero).orElse(null);
	}

	@Override
	public List<Client> rechercherTousClients() {
		return daoClient.findAll();
	}

	@Override
	public List<Compte> rechercherComptesDuClient(long numClient) {
		// sera codé plus tard
		return null;
	}

	@Override
	public Client sauvegarderClient(Client Client) {
		return daoClient.save(Client);
	}

	@Override
	public void supprimerClient(long numCpt) {
		daoClient.deleteById(numCpt);
	}

	@Override
	public Client rechercherClientParNumeroInclusComptes(long numero) {
		return daoClient.findWithCompteById(numero);
	}
}