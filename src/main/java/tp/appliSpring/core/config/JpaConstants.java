package tp.appliSpring.core.config;

public class JpaConstants {
	/* COMPTES */
	// Query names
	public final static String compteFindAll = "Compte.findAll";
	public final static String compteFindByClientId = "Compte.findByClientId";
	public final static String compteFindWithOperationsById = "Compte.findWithOperationsById";

	// Queries

	public final static String compteFindAllQuery = "SELECT c FROM Compte c";
	public final static String compteFindByClientIdQuery = "SELECT c FROM Compte c JOIN c.clients cli WHERE cli.numero = ?1";
	public final static String compteFindWithOperationsByIdQuery = "SELECT c FROM Compte c LEFT JOIN FETCH c.operations WHERE c.numero = ?1";

	/* CLIENTS */
	// Query names
	public final static String clientFindAll = "Client.findAll";
	public final static String clientFindClientByIdIncludeAccounts = "Client.findWithCompteById";
	// Queries
	public final static String clientFindAllQuery = "SELECT c FROM Client c";
	public final static String clientFindClientByIdIncludeAccountsQuery = "SELECT c FROM Client c LEFT JOIN FETCH c.comptes com WHERE c.numero = ?1";

	/* OPERATIONS */
	// Query names
	public final static String operationFindAll = "Operation.findAll";
	public final static String operationFindOperationsByAccountId = "Operation.findOperationsByAccountId";
	// Queries
	public final static String operationFindAllQuery = "SELECT o FROM Operation o";
	public final static String operationFindOperationsByAccountIdQuery = "SELECT o FROM Operation o WHERE compte.numero= ?1";
}
