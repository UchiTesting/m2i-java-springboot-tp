package tp.appliSpring.core.config;

//@Configuration
public class WebSecurityConfig
//extends WebSecurityConfigurerAdapter 
{
}
/*
 * 
 * @Bean public BCryptPasswordEncoder passwordEncoder() { return
 * passwordEncoder; }
 * 
 * //@Autowired private BCryptPasswordEncoder passwordEncoder = new
 * BCryptPasswordEncoder();
 * 
 * @Autowired public void globalUserDetails(final AuthenticationManagerBuilder
 * auth) throws Exception {
 * auth.inMemoryAuthentication().withUser("user1").password(passwordEncoder.
 * encode("pwd1")).roles("USER").and()
 * .withUser("admin1").password(passwordEncoder.encode("pwd1")).roles("ADMIN").
 * and().withUser("user2")
 * .password(passwordEncoder.encode("pwd2")).roles("USER").and().withUser(
 * "admin2") .password(passwordEncoder.encode("pwd2")).roles("ADMIN"); }
 * 
 * @Override
 * 
 * @Bean public AuthenticationManager authenticationManagerBean() throws
 * Exception { return super.authenticationManagerBean(); }
 * 
 * @Override protected void configure(final HttpSecurity http) throws Exception
 * {
 * 
 * // Config pour Spring-mvc avec pages jsp : // http.authorizeRequests() //
 * .antMatchers("/", "/favicon.ico", "/\*\*\/*.png", "/**\/*.gif", "/**\/*.svg",
 * "/**\/*.jpg", "/**\/*.html", // "/**\/*.css", "/**\/*.js") //
 * .permitAll().anyRequest().authenticated().and().formLogin().permitAll().and()
 * .csrf();
 * 
 * // Config pour Spring-mvc avec WS-REST et tokens (ex : jwt) :
 * http.authorizeRequests() // .antMatchers("/rest/**").permitAll()
 * .anyRequest().permitAll() // .anyRequest().authenticated() //
 * .anyRequest().hasRole("ADMIN") .and().csrf().disable(); // .and().httpBasic()
 * } } //
 */