package tp.appliSpring.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tp.appliSpring.core.entity.Client;

/**
 *
 * Avec Spring Data plus besoin de coder les classes d'implémentation *Dao...Jpa*
 * 
 */

public interface IDaoClient extends JpaRepository<Client, Long> {
	/*
	 * Methods héritées de JpaRepository ou CrudRepository
	 * 
	 * Client findById(Long numClt); // Renvoi Optional<Client>
	 * 
	 * Client save(Client client); // sauvegarde au sens saveOrUpdate
	 * 
	 * List<Client> findAll();
	 * 
	 * void deleteById(Long numClt);
	 */

//	Client findByIdIncludeComptes(long numero);
	Client findWithCompteById(long numero);
}
