package tp.appliSpring.core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import tp.appliSpring.core.entity.Compte;

public interface IDaoCompte extends JpaRepository<Compte, Long> {
	/*
	 * Compte findById(Long numCpt);
	 * 
	 * Compte save(Compte compte); // sauvegarde au sens saveOrUpdate
	 * 
	 * List<Compte> findAll();
	 * 
	 * 
	 * void deleteById(Long numCpt);
	 */
	List<Compte> findByClientId(long id);

	List<Compte> findBySoldeGreaterThanEqual(double soldeMin);
	
	Compte findWithOperationsById(Long numCompte);
}
