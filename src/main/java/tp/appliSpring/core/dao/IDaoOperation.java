package tp.appliSpring.core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import tp.appliSpring.core.entity.Operation;

public interface IDaoOperation extends JpaRepository<Operation, Long>{
	/*
	 * Operation findById(Long numOpt);
	 * 
	 * Operation save(Operation operation);
	 * 
	 * List<Operation> findAll();
	 * 
	 * 
	 * void deleteById(Long numOpt);
	 */
	List<Operation> findOperationsByCompteNumero(long numCompte);
}
