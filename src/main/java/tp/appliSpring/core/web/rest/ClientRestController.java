package tp.appliSpring.core.web.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tp.appliSpring.core.converter.DtoConverter;
import tp.appliSpring.core.dto.ClientDto;
import tp.appliSpring.core.dto.Message;
import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.service.IServiceClient;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/bank-api/client", headers = "Accept=application/json")
public class ClientRestController {

	@Autowired
	IServiceClient serviceClient;

	// URL http://localhost:8080/appliSpringBoot/bank-api/client/{numClient}
	@GetMapping(value = "/{numClient}")
	public ClientDto getClientDtoByNum(@PathVariable("numClient") Long numClient) {
		return DtoConverter.clientToClientDto(serviceClient.rechercherClientParNumero(numClient));
	}

	// URL= http://localhost:8080/appliSpringBoot/bank-api/client
	// ou http://localhost:8080/appliSpringBoot/bank-api/client?nom=Therieur
	@GetMapping("")
	public List<ClientDto> getClientDtoListByCriteria(@RequestParam(required = false, name = "nom") String nomClient) {
		List<ClientDto> clientDtoList = DtoConverter.clientListToClientDtoList(serviceClient.rechercherTousClients());

		if (nomClient != null)
			return clientDtoList.stream().filter(c -> c.getNom().equalsIgnoreCase(nomClient))
					.collect(Collectors.toList());

		// return serviceClient.rechercherClientParNom(nomClient); // Bonne solution
		return clientDtoList;
	}

	// URL= POST http://localhost:8080/appliSpringBoot/bank-api/client
	// Le corps de la requête contiendra des données JSON.
	// { numero: null, nom: "Nom",prenom: "Prenom", adresse: "Adresse", email:
	// "e-mail"}
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("")
	public ClientDto postClient(@RequestBody ClientDto clientDto) {
		Client client = DtoConverter.clientDtoToClient(clientDto);
		serviceClient.sauvegarderClient(client);
		clientDto.setNumero(client.getNumero());
		return clientDto;
	}

	// URL= PUT http://localhost:8080/appliSpringBoot/bank-api/client
	// Le corps de la requête contiendra des données JSON.
	// La différence avec POST est que l'ID est connu donc on le précise dans le
	// JSON
	// { numero: 3, nom: "Nom",prenom: "Prenom", adresse: "Adresse", email:
	// "e-mail"}
	@PutMapping("")
	public ResponseEntity<?> putClient(@RequestBody ClientDto clientDto) {
		Client client = DtoConverter.clientDtoToClient(clientDto);
		Client clientInDb = serviceClient.rechercherClientParNumeroInclusComptes(clientDto.getNumero());

		if (clientInDb == null)
			return new ResponseEntity<Message>(new Message("Client " + clientDto.getNumero() + " does not exist."),
					HttpStatus.NOT_FOUND);

		serviceClient.sauvegarderClient(client);
		clientDto.setNumero(client.getNumero());
		return new ResponseEntity<ClientDto>(clientDto, HttpStatus.OK);
	}

	// URL= DELETE http://localhost:8080/appliSpringBoot/bank-api/client
	// Le corps de la requête contiendra des données JSON.
	// { numero: null, nom: "Nom",prenom: "Prenom", adresse: "Adresse", email:
	// "e-mail"}
	@DeleteMapping("/{numClient}")
	public ResponseEntity<Message> deleteClient(@PathVariable("numClient") Long numClient) {
		try {
			serviceClient.supprimerClient(numClient);
			return new ResponseEntity<Message>(new Message("Client " + numClient + " deleted successfully."),HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Message>(new Message("Client " + numClient + " could not be deleted."),
					HttpStatus.NOT_FOUND);
		}
	}
}