package tp.appliSpring.core.web.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tp.appliSpring.core.converter.DtoConverter;
import tp.appliSpring.core.dto.CompteDto;
import tp.appliSpring.core.dto.CompteDtoEx;
import tp.appliSpring.core.dto.Message;
import tp.appliSpring.core.dto.VirementDto;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.exception.NotFoundException;
import tp.appliSpring.core.exception.SoldeInsuffisantException;
import tp.appliSpring.core.service.IServiceCompte;

@CrossOrigin(origins = { "http://localhost:4200" , "http://sample.partner-site.com" })
@RestController
@RequestMapping(value = "/bank-api/compte", headers = "Accept=application/json")
public class CompteRestController {

	@Autowired
	IServiceCompte serviceCompte;

	// URL http://localhost:8080/appliSpringBoot/bank-api/compte/{numCompte}
	@GetMapping("/{numCompte}")
	public CompteDto getCompteByNum(@PathVariable("numCompte") Long numCompte) throws NotFoundException {
		Compte compteEntity = serviceCompte.rechercherCompteParNumero(numCompte);
		CompteDto compteDto = DtoConverter.compteToCompteDto(compteEntity);
		return compteDto;
	}

	// URL http://localhost:8080/appliSpringBoot/bank-api/compte/ex/{numCompte}
	@GetMapping(value = "/ex/{numCompte}")
	public CompteDtoEx getCompteDtoByNumWithDetails(@PathVariable("numCompte") Long numCompte) {
		return DtoConverter.compteToCompteDtoEx(serviceCompte.rechercherCompteAvecOperationsParNumero(numCompte));
	}

	// URL= http://localhost:8080/appliSpringBoot/bank-api/compte
	// ou http://localhost:8080/appliSpringBoot/bank-api/compte?numClient=1
	// ou http://localhost:8080/appliSpringBoot/bank-api/compte?soldeMini=50
	@GetMapping("")
	public List<CompteDto> getCompteDtoListByCriteria(
			@RequestParam(required = false, name = "numClient") Long numClient,
			@RequestParam(required = false, name = "soldeMini") Long soldeMini) {
		List<CompteDto> clientDtoList;

		if (numClient != null)
			clientDtoList = DtoConverter.compteListToCompteDtoList(serviceCompte.rechercherComptesDuClient(numClient));
		else
			clientDtoList = DtoConverter.compteListToCompteDtoList(serviceCompte.rechercherTousComptes());

		if (soldeMini != null)
			clientDtoList = clientDtoList.stream().filter(c -> c.getSolde() >= soldeMini).collect(Collectors.toList());

		return clientDtoList;
	}

	// URL= POST http://localhost:8080/appliSpringBoot/bank-api/compte
	@PostMapping("")
	public CompteDto postCompte(@RequestBody CompteDto compteDto) {
		Compte compte = DtoConverter.compteDtoToCompte(compteDto);

		serviceCompte.sauvegarderCompte(compte);
		compteDto.setNumero(compte.getNumero());
		return compteDto;
	}

	// URL= POST http://localhost:8080/appliSpringBoot/bank-api/compte/virement
	@PostMapping("virement")
	public ResponseEntity<?> postVirementCompte(@RequestBody VirementDto virementDto) {
		try {
			serviceCompte.transferer(virementDto.getMontant(), Long.parseLong(virementDto.getNumCptDebt()),
					Long.parseLong(virementDto.getNumCptCred()));
			virementDto.setStatus(true);
			virementDto.setMessage("Virement effectué.");
		} catch (SoldeInsuffisantException sie) {
			virementDto.setStatus(false);
			virementDto.setMessage("Virement non effectué. " + sie.getMessage());
			sie.printStackTrace();
		} catch (NumberFormatException nfe) {
			virementDto.setStatus(false);
			virementDto.setMessage("Virement non effectué. Vérifiez le numéro de compte.");
			nfe.printStackTrace();
		} catch (Exception e) {
			virementDto.setStatus(false);
			virementDto.setMessage("Virement non effectué. " + e.getMessage());

		}

		return new ResponseEntity<VirementDto>(virementDto, HttpStatus.OK);
	}

	// URL= POST http://localhost:8080/appliSpringBoot/bank-api/compte
	@PutMapping("")
	public ResponseEntity<?> putCompte(@RequestBody CompteDto compteDto) {
		try {
			Compte compteInDb = serviceCompte.rechercherCompteParNumero(compteDto.getNumero());
			Compte compte = DtoConverter.compteDtoToCompte(compteDto);
			serviceCompte.sauvegarderCompte(compte);
			compteDto.setNumero(compte.getNumero());

			return new ResponseEntity<CompteDto>(compteDto, HttpStatus.CREATED);
		} catch (NotFoundException e) {
			return new ResponseEntity<Message>(new Message("Account " + compteDto.getNumero() + " not found."),
					HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{numCompte}")
	public ResponseEntity<Message> deleteCompte(@PathVariable("numCompte") Long numCompte) {
		try {
			serviceCompte.supprimerCompte(numCompte);
			return new ResponseEntity<Message>(new Message("Compte " + numCompte + " deleted successfully."),
					HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Message>(new Message("Compte " + numCompte + " could not be deleted."),
					HttpStatus.NOT_FOUND);
		}
	}

}
