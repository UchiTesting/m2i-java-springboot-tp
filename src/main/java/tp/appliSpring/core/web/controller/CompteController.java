package tp.appliSpring.core.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.entity.Operation;
import tp.appliSpring.core.service.IServiceClient;
import tp.appliSpring.core.service.IServiceCompte;
import tp.appliSpring.core.service.IServiceOperation;

@Controller
@SessionAttributes(value = { "client" })
public class CompteController {

	@Autowired
	private IServiceCompte serviceCompte;
	@Autowired
	private IServiceClient serviceClient;
	@Autowired
	private IServiceOperation serviceOperation;

	@ModelAttribute("client") // NB: cette méthode n'est pas appelée/déclenchée
	// si "client" est déjà présent en session (et par copie) dans le modèle
	public Client addClientAttributeInModel() {
		return new Client();
	}

	@RequestMapping("/versLogin")
	public String versLogin(Model model) {
		return "login";
	}

	@RequestMapping("/verifLogin")
	public String verifLogin(Model model, @RequestParam(name = "numClient") Long numClient) {
		List<Compte> comptes = serviceCompte.rechercherComptesDuClient(numClient);
		Client client = serviceClient.rechercherClientParNumero(numClient);
		model.addAttribute("comptes", comptes);
		model.addAttribute("client", client);
		return "comptes";
	}

	@RequestMapping("/operationsCompte")
	public String versOperationCompte(Model model, @RequestParam(name = "numCompte") Long numCompte) {
		Compte compte = serviceCompte.rechercherCompteParNumero(numCompte);
		List<Operation> operations = serviceOperation.rechercherOperationsDuCompte(numCompte);

		model.addAttribute("operations", operations);
		model.addAttribute("compte", compte);

		return "operations";
	}
	
	@RequestMapping("/versVirement")
	 public String versVirement(Model model) {
	    return "virement"; //aiguiller sur la vue "virement"
	    //selon la config de application.properties jsp/virement.jsp
	 }

	@RequestMapping("/effectuerVirement")
	public String effectuerVirement(Model model, @RequestParam(name = "montant") Double montant,
			@RequestParam(name = "numCptDeb") Long numCptDeb, @RequestParam(name = "numCptCred") Long numCptCred) {
		String message = "";
		try {
			serviceCompte.transferer(montant, numCptDeb, numCptCred);
			message = "virement bien effectué";
		} catch (Exception e) {
			message = "echec virement: " + e.getMessage();
			e.printStackTrace();
		}
		Client client = (Client) model.getAttribute("client");
		List<Compte> comptes = serviceCompte.rechercherComptesDuClient(client.getNumero());
		model.addAttribute("client", client);
		model.addAttribute("comptes", comptes);
		model.addAttribute("message", message);
		return "comptes"; // aiguiller sur la vue "comptes"
		// selon la config de application.properties jsp/comptes.jsp
	}
}
