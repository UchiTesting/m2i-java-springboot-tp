package tp.appliSpring.core.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {

	@RequestMapping("/helloWorld")
	public String helloWorld(Model model) {
		model.addAttribute("message", "Hello World!");
		// Aiguille sur la vue showMessage
		// Selon la config jsp/showMessage.jsp
		return "showMessage";
	}

}
