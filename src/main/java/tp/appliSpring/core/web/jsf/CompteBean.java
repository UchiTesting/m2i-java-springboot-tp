package tp.appliSpring.core.web.jsf;

import java.util.List;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.RequestScope;

import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.service.IServiceCompte;

@ManagedBean
@RequestScope
public class CompteBean {
	private Long numClient;
	private List<Compte> comptes;

	@Autowired
	private IServiceCompte serviceCompte;

	public Long getNumClient() {
		return numClient;
	}

	public void setNumClient(Long numClient) {
		this.numClient = numClient;
	}

	public List<Compte> getComptes() {
		return comptes;
	}

	public void setComptes(List<Compte> comptes) {
		this.comptes = comptes;
	}

	public IServiceCompte getServiceCompte() {
		return serviceCompte;
	}

	public void setServiceCompte(IServiceCompte serviceCompte) {
		this.serviceCompte = serviceCompte;
	}

	public String doLogin() {
		comptes = serviceCompte.rechercherComptesDuClient(this.numClient);
		return null;
	}
}
