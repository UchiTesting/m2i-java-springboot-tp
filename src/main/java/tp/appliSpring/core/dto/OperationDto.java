package tp.appliSpring.core.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class OperationDto {
	Long numOp;
	String label;
	Double montant;
	String dateOp; // On a utilisé un type différent

	public OperationDto(Long numOp, String label, Double montant, String dateOp) {
		super();
		this.numOp = numOp;
		this.label = label;
		this.montant = montant;
		this.dateOp = dateOp;
	}

}
