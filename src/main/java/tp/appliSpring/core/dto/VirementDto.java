package tp.appliSpring.core.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class VirementDto {
	//
	private String numCptDebt;
	private String numCptCred;
	private double montant;

	private boolean status;
	private Date date;
	private String message;

	public VirementDto(String numCptDebt, String numCptCred, double montant) {
		super();
		this.numCptDebt = numCptDebt;
		this.numCptCred = numCptCred;
		this.montant = montant;
	}

	public VirementDto(String numCptDebt, String numCptCred, double montant, boolean status, Date date,
			String message) {
		super();
		this.numCptDebt = numCptDebt;
		this.numCptCred = numCptCred;
		this.montant = montant;
		this.status = status;
		this.date = date;
		this.message = message;
	}
}
