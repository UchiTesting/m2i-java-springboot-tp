package tp.appliSpring.core.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

// entity.Client => dto.ClientDto
// entity.Client <= dto.ClientDto
// Annotations Lombok
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ClientDto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long numero;
	private String nom;
	private String prenom;
	private String adresse;
	private String email;

	public ClientDto(Long numero, String nom, String prenom, String adresse, String email) {
		super();
		this.numero = numero;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.email = email;
	}
}
