package tp.appliSpring.core.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import tp.appliSpring.core.dto.ClientDto;
import tp.appliSpring.core.dto.CompteDto;
import tp.appliSpring.core.dto.CompteDtoEx;
import tp.appliSpring.core.dto.OperationDto;
import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.entity.Operation;

public class DtoConverter {

	public static ClientDto clientToClientDto(Client client) {
		return new ClientDto(client.getNumero(), client.getNom(), client.getPrenom(), client.getAdresse(),
				client.getEmail());
	}

	public static Client clientDtoToClient(ClientDto clientDto) {
		return new Client(clientDto.getNumero(), clientDto.getNom(), clientDto.getPrenom(), clientDto.getAdresse(),
				clientDto.getEmail());
	}

	public static List<ClientDto> clientListToClientDtoList(List<Client> clientList) {
		return clientList.stream().map(c -> clientToClientDto(c)).collect(Collectors.toList());
	}

	public static CompteDto compteToCompteDto(Compte compte) {
		CompteDto compteDto = new CompteDto();

		compteDto.setNumero(compte.getNumero());
		compteDto.setLabel(compte.getLabel());
		compteDto.setSolde(compte.getSolde());

		return compteDto;
	}

	public static Compte compteDtoToCompte(CompteDto compteDto) {
		return new Compte(compteDto.getNumero(), compteDto.getLabel(), compteDto.getSolde());
	}

	public static List<CompteDto> compteListToCompteDtoList(List<Compte> compteList) {
		return compteList.stream().map(c -> compteToCompteDto(c)).collect(Collectors.toList());
	}

	public static CompteDtoEx compteToCompteDtoEx(Compte compte) {
		CompteDtoEx compteDtoEx = new CompteDtoEx();
		BeanUtils.copyProperties(compte, compteDtoEx);

		for (Operation op : compte.getOperations()) {
			OperationDto opDto = new OperationDto(op.getNumOp(), op.getLabel(), op.getMontant(),
					op.getDateOp().toString());
			compteDtoEx.getOperations().add(opDto);
		}

		return compteDtoEx;
	}
}
