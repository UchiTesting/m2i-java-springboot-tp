package tp.appliSpring.core.init;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.service.IServiceClient;
import tp.appliSpring.core.service.IServiceCompte;

@Component
@Profile("dev")
public class ReInitDataSet {
	@Autowired
	private IServiceCompte serviceCompte;

	@Autowired
	private IServiceClient serviceClient;

	@PostConstruct
	public void init() {
		Compte compte1 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 1", 100.0));
		Compte compte2 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 2", 200.0));
		Compte compte3 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 3", 300.0));
		Compte compte4 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 4", 450.5));
		Compte compte5 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 5", 500.0));

		Client client1 = serviceClient
				.sauvegarderClient(new Client(null, "Client", "Un", "Adresse Client 1", "un@domain.com"));
		Client client2 = serviceClient
				.sauvegarderClient(new Client(null, "Client", "Deux", "Adresse Client 2", "deux@domain.com"));

		client1.getComptes().add(compte1);
		client1.getComptes().add(compte3);
		client1.getComptes().add(compte5);
		serviceClient.sauvegarderClient(client1);

		client2.getComptes().add(compte2);
		client2.getComptes().add(compte4);
		serviceClient.sauvegarderClient(client2);

	}
}
