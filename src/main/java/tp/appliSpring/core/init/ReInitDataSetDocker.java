package tp.appliSpring.core.init;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import tp.appliSpring.core.entity.Client;
import tp.appliSpring.core.entity.Compte;
import tp.appliSpring.core.entity.Operation;
import tp.appliSpring.core.service.IServiceClient;
import tp.appliSpring.core.service.IServiceCompte;
import tp.appliSpring.core.service.IServiceOperation;

@Component
@Profile("docker")
public class ReInitDataSetDocker {
	@Autowired
	private IServiceCompte serviceCompte;

	@Autowired
	private IServiceClient serviceClient;

	@Autowired
	private IServiceOperation serviceOperation;

	@PostConstruct
	public void init() {
		Compte compte1 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 1", 100.0));
		Compte compte2 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 2", 200.0));
		Compte compte3 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 3", 300.0));
		Compte compte4 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 4", 450.5));
		Compte compte5 = serviceCompte.sauvegarderCompte(new Compte(null, "Compte 5", 500.0));

		Client client1 = serviceClient
				.sauvegarderClient(new Client(null, "Client", "Un", "Adresse Client 1", "un@domain.com"));
		Client client2 = serviceClient
				.sauvegarderClient(new Client(null, "Client", "Deux", "Adresse Client 2", "deux@domain.com"));
		Client client3 = serviceClient
				.sauvegarderClient(new Client(null, "Usager", "Trois", "Adresse Usager 3", "trois@domain.com"));

		client1.getComptes().add(compte1);
		client1.getComptes().add(compte3);
		client1.getComptes().add(compte5);
		serviceClient.sauvegarderClient(client1);

		client2.getComptes().add(compte2);
		client2.getComptes().add(compte4);
		serviceClient.sauvegarderClient(client2);

		Operation operation01 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 01", 10.0, new Date(), compte1));
		Operation operation02 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 02", 12.9, new Date(), compte1));
		Operation operation03 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 03", 410.5, new Date(), compte1));
		Operation operation04 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 04", -54.0, new Date(), compte1));
		Operation operation05 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 05", 11.11, new Date(), compte1));
		Operation operation06 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 06", 10.0, new Date(), compte2));
		Operation operation07 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 07", 102.23, new Date(), compte2));
		Operation operation08 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 08", 210.30, new Date(), compte2));
		Operation operation09 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 09", 880.02, new Date(), compte2));
		Operation operation10 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 10", 22.22, new Date(), compte2));
		Operation operation11 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 11", 10.0, new Date(), compte3));
		Operation operation12 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 12", 0.99, new Date(), compte3));
		Operation operation13 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 13", 2.0, new Date(), compte3));
		Operation operation14 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 14", 5010.92, new Date(), compte3));
		Operation operation15 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 15", 33.33, new Date(), compte3));
		Operation operation16 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 16", 10.0, new Date(), compte4));
		Operation operation17 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 17", 40.0, new Date(), compte4));
		Operation operation18 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 18", 1021.47, new Date(), compte4));
		Operation operation19 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 19", 92.36, new Date(), compte4));
		Operation operation20 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 20", 44.44, new Date(), compte4));
		Operation operation21 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 21", 10.0, new Date(), compte5));
		Operation operation22 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 22", 50.0, new Date(), compte5));
		Operation operation23 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 23", 1000.50, new Date(), compte5));
		Operation operation24 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 24", 37.52, new Date(), compte5));
		Operation operation25 = serviceOperation
				.sauvegarderOperation(new Operation(null, "Operation 25", 55.55, new Date(), compte5));
	}
}
