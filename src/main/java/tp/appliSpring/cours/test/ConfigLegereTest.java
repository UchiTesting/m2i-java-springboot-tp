package tp.appliSpring.cours.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("tp.appliSpring.cours.test")
public class ConfigLegereTest {

}
