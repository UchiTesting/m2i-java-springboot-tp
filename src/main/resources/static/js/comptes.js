function searchComptes() {
    let numClient = document.getElementById("txtNumClient").value;
    let wsUrl = "./bank-api/compte?numClient=" + numClient;
    makeAjaxGetRequest(wsUrl, (response) => {
        let jsComptes = JSON.parse(response);
        // document.getElementById("resComptes").innerHTML = jsComptes;
        document.getElementById("resComptes").innerHTML = response;
        let tbodyElem = document.getElementById("tbody");

        tbodyElem.innerHTML = "";

        for (let i in jsComptes) {
            let compte = jsComptes[i];
            let trElem = tbodyElem.insertRow(-1);
            trElem.insertCell(0).innerHTML = compte.numero;
            trElem.insertCell(1).innerHTML = compte.label;
            trElem.insertCell(2).innerHTML = compte.solde;
        }
    });
}