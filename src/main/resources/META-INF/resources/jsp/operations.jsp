<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Operations du compte</title>
<link rel="stylesheet" href="css/styles.css" />
</head>
<body>
	<h2>Informations du compte</h2>
	<div class="client-info-container">
		<p>
			<span class="data-header">Numero: </span><span class="data-value">${compte.numero}</span>
		</p>
		<p>
			<span class="data-header">Label: </span><span class="data-value">${compte.label}</span>
		</p>
		<p>
			<span class="data-header">Solde: </span><span class="data-value">${compte.solde}</span>
		</p>
		<p>
			<span class="data-header">Propri&eacute;taire: </span><span
				class="data-value">client.nom</span>
		</p>
	</div>
	<h2>Operations du compte</h2>
	<table border="1">
		<tr>
			<th>Numero</th>
			<th>Date</th>
			<th>Label</th>
			<th>Montant</th>
		</tr>
		<c:forEach var="o" items="${operations}">
			<tr>
				<td>${o.numOp}</td>
				<td>${o.dateOp}</td>
				<td>${o.label}</td>
				<td>${o.montant}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>