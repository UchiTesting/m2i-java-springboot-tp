<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des comptes</title>
<link rel="stylesheet" href="css/styles.css" />
</head>
<body>
	<h2>Informations du client</h2>
	
	<form action="effectuerVirement" method="POST">
		<input name="montant" placeholder="Montant" /><br/>
		<input name="numCptDeb" placeholder="Numéro de compte à débiter" /><br/>
		<input name="numCptCred" placeholder="Numéro de compte à créditer" /><br/>
		<button type="submit">Submit</button>
	</form>
	
	
	<div class="client-info-container">
		<p>
			<span class="data-header">Nom: </span><span class="data-value">${client.nom}</span>
		</p>
		<p>
			<span class="data-header">Prenom: </span><span class="data-value">${client.prenom}</span>
		</p>
		<p>
			<span class="data-header">Adresse: </span><span class="data-value">${client.adresse}</span>
		</p>
		<p>
			<span class="data-header">e-mail: </span><span class="data-value">${client.email}</span>
		</p>
	</div>
	<h2>Comptes du client</h2>
	<table border="1">
		<tr>
			<th>Numero</th>
			<th>Label</th>
			<th>Solde</th>
		</tr>
		<c:forEach var="c" items="${comptes}">
			<tr>
				<td>${c.numero}</td>
				<td><a href="operationsCompte?numCompte=${c.numero}">${c.label}</a></td>
				<td>${c.solde}</td>
			</tr>
		</c:forEach>
	</table>
	<a href="index.html">Index</a><br/>
</body>
</html>